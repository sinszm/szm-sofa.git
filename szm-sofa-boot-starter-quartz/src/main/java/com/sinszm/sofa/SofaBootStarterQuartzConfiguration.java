package com.sinszm.sofa;


import cn.hutool.core.net.NetUtil;
import com.sinszm.sofa.exception.ApiException;
import com.sinszm.sofa.job.CommandLineRunnerJob;
import com.sinszm.sofa.response.StatusCode;
import com.sinszm.sofa.service.IQuartzService;
import com.sinszm.sofa.util.BaseUtil;
import com.xxl.job.core.executor.impl.XxlJobSpringExecutor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.quartz.QuartzDataSourceInitializer;
import org.springframework.boot.autoconfigure.quartz.QuartzProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.ResourceLoader;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.scheduling.quartz.SpringBeanJobFactory;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Properties;

/**
 * 配置加载中心
 * @author sinszm
 */
@Slf4j
@EnableConfigurationProperties(SzmQuartzProperties.class)
public class SofaBootStarterQuartzConfiguration implements CommandLineRunner {

    @Resource
    private ApplicationContext context;

    @Resource
    private SzmQuartzProperties szmQuartzProperties;

    /**
     * 数据源
     *
     * @return {DataSource}
     */
    @Deprecated
    private DataSource dataSource() {
        try {
            return context.getBean(szmQuartzProperties.checkAll().getDataSource(), DataSource.class);
        } catch (BeansException e) {
            log.error("数据源读取异常!", e);
            throw new ApiException(StatusCode.SYSTEM_ERROR);
        }
    }

    /**
     * 任务调度表结构数据源初始化
     *
     * @param resourceLoader 资源加载器
     * @param properties     属性
     * @return {QuartzDataSourceInitializer}
     */
    @Deprecated
    @Bean
    @ConditionalOnProperty(prefix = "quartz", name = "xxl", havingValue = "false", matchIfMissing = true)
    @ConditionalOnMissingBean
    public QuartzDataSourceInitializer quartzDataSourceInitializer(ResourceLoader resourceLoader, QuartzProperties properties) {
        properties.getJdbc().setInitializeSchema(szmQuartzProperties.checkAll().getMode());
        properties.getJdbc().setSchema("classpath:org/quartz/impl/jdbcjobstore/tables_@@platform@@.sql");
        return new QuartzDataSourceInitializer(dataSource(), resourceLoader, properties);
    }

    /**
     * quartz调度器
     *
     * @return {SchedulerFactoryBean}
     */
    @Deprecated
    @Primary
    @ConditionalOnProperty(prefix = "quartz", name = "xxl", havingValue = "false", matchIfMissing = true)
    @Bean
    public SchedulerFactoryBean schedulerFactoryBean() {
        SchedulerFactoryBean bean = new SchedulerFactoryBean();
        bean.setDataSource(dataSource());
        bean.setJobFactory(new SpringBeanJobFactory());
        bean.setQuartzProperties(quartzProperties());
        return bean;
    }

    /**
     * xxl工作执行器注册
     *
     * @return {@link XxlJobSpringExecutor}
     */
    @Bean
    @Primary
    @ConditionalOnProperty(prefix = "quartz", name = "xxl", havingValue = "true")
    public XxlJobSpringExecutor xxlJobSpringExecutor() {
        SzmQuartzProperties.JobExecutorProperties prop = szmQuartzProperties.checkAll().getXxlJob();
        XxlJobSpringExecutor executor = new XxlJobSpringExecutor();
        executor.setAdminAddresses(prop.getAdminAddress());
        executor.setAccessToken(prop.getAccessToken());
        executor.setAppname(prop.getExecutorAppName());
        if (!BaseUtil.isEmpty(prop.getExecutorAddress())) {
            executor.setAddress(prop.getExecutorAddress());
        }
        executor.setIp(BaseUtil.isEmpty(prop.getExecutorIp()) ? NetUtil.getLocalhostStr() : prop.getExecutorIp());
        executor.setPort(prop.getExecutorPort());
        executor.setLogPath(prop.getExecutorLogPath());
        executor.setLogRetentionDays(prop.getExecutorLogRetentionDays());
        return executor;
    }

    /**
     * 定时任务配置
     *
     * @return {Properties}
     */
    @Deprecated
    private Properties quartzProperties() {
        Properties properties = new Properties();
        properties.setProperty("org.quartz.scheduler.instanceName", szmQuartzProperties.checkAll().getInstanceName());
        properties.setProperty("org.quartz.scheduler.instanceId", "AUTO");
        properties.setProperty("org.quartz.jobStore.class", "org.quartz.impl.jdbcjobstore.JobStoreTX");
        properties.setProperty("org.quartz.jobStore.driverDelegateClass", "org.quartz.impl.jdbcjobstore.StdJDBCDelegate");
        properties.setProperty("org.quartz.jobStore.tablePrefix", "QRTZ_");
        properties.setProperty("org.quartz.jobStore.isClustered", "true");
        properties.setProperty("org.quartz.jobStore.clusterCheckinInterval", "10000");
        properties.setProperty("org.quartz.threadPool.class", "org.quartz.simpl.SimpleThreadPool");
        properties.setProperty("org.quartz.threadPool.threadCount", "20");
        properties.setProperty("org.quartz.threadPool.threadPriority", "5");
        properties.setProperty("org.quartz.threadPool.threadsInheritContextClassLoaderOfInitializingThread", "true");
        return properties;
    }

    private static final String DEFAULT_NAME = "_job";
    private static final String DEFAULT_GROUP = "_job_group";
    private static final String DEFAULT_CRON = "0/10 * * * * ? *";

    /**
     * 任务创建于监听
     * @param args  参数
     */
    @Deprecated
    @SneakyThrows
    @Override
    public void run(String... args) {
        if (szmQuartzProperties.isXxl()) {
            log.info("启用了xxl-job任务调度模式。");
            return;
        }
        //是否启用任务扫描
        if (!szmQuartzProperties.isScanTask()) {
            return;
        }
        IQuartzService iQuartzService = context.getBean(IQuartzService.class);
        String name = (context.getEnvironment().getProperty("spring.application.name") + DEFAULT_NAME).replace("-", "_").toUpperCase();
        String group = (context.getEnvironment().getProperty("spring.application.name") + DEFAULT_GROUP).replace("-", "_").toUpperCase();
        if (!iQuartzService.checkExists(name, group)) {
            iQuartzService.addJob(name, group, CommandLineRunnerJob.class, DEFAULT_CRON, new HashMap<>(0));
        }
    }

}