package com.sinszm.sofa.annotation;

import java.lang.annotation.*;

/**
 * JOB注解
 *
 * @author fh411
 */
@Deprecated
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface QJob {

    /**
     * 分组
     */
    String group() default "";

    /**
     * 名称
     */
    String name() default "";

    /**
     * cron表达式
     */
    String cron() default "";

    /**
     * 默认任务执行的方法名称
     */
    String methodName() default "execute";

}
