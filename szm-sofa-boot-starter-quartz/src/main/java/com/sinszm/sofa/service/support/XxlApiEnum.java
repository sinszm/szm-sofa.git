package com.sinszm.sofa.service.support;

/**
 * xxl api枚举
 *
 * @author admin
 * @date 2022/02/21
 */
public enum XxlApiEnum {

    /**
     * 新增任务
     */
    ADD_JOB("open/addJob"),
    /**
     * 修改任务
     */
    UPDATE_JOB("open/updateJob"),
    /**
     * 删除任务
     */
    REMOVE_JOB("open/removeJob"),
    /**
     * 暂停任务
     */
    PAUSE_JOB("open/pauseJob"),
    /**
     * 开始任务
     */
    START_JOB("open/startJob"),
    /**
     * 新增并开始任务
     */
    ADD_AND_START_JOB("open/addAndStart"),
    /**
     * 获取任务执行器ID
     */
    GET_GROUP_ID("open/getGroupId"),
    ;

    XxlApiEnum(String api) {
        this.api = api;
    }

    private final String api;

    public String getApi() {
        return api;
    }

}
