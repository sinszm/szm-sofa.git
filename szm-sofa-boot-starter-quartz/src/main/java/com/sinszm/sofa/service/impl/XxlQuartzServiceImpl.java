package com.sinszm.sofa.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.extra.spring.SpringUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.sinszm.sofa.SzmQuartzProperties;
import com.sinszm.sofa.exception.ApiException;
import com.sinszm.sofa.service.XxlQuartzService;
import com.sinszm.sofa.service.support.XxlApiEnum;
import com.sinszm.sofa.util.BaseUtil;
import com.xxl.job.core.executor.XxlJobExecutor;
import com.xxl.job.core.handler.IJobHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * xxl任务调度服务实现
 *
 * @author fh411
 * @date 2022/02/18
 */
@Slf4j
@ConditionalOnProperty(prefix = "quartz", name = "xxl", havingValue = "true")
@Service
public class XxlQuartzServiceImpl implements XxlQuartzService {

    private final SzmQuartzProperties szmQuartzProperties;

    private static final String XXL_JOB_REQ_KEY = "xxl-job-req-key";

    public XxlQuartzServiceImpl(SzmQuartzProperties szmQuartzProperties) {
        this.szmQuartzProperties = szmQuartzProperties;
    }

    private SzmQuartzProperties.JobExecutorProperties config() {
        return szmQuartzProperties.checkAll().getXxlJob();
    }

    private List<String> adminAddress() {
        String adminAddress = config().getAdminAddress();
        return StrUtil.splitTrim(
                BaseUtil.trim(adminAddress)
                        .replace("，",",")
                        .replace(";",",")
                        .replace("；",","),
                ',').stream()
                .map(addressUrl -> {
                    if (!addressUrl.endsWith("/")) {
                        return addressUrl + "/";
                    }
                    return addressUrl;
                })
                .collect(Collectors.toList());
    }

    private String reqKey() {
        return SecureUtil.aes(BaseUtil.trim(config().getAccessToken()).getBytes())
                .encryptHex(System.currentTimeMillis() + "");
    }

    private JSONObject post(XxlApiEnum api, Map<String,Object> params) {
        JSONObject result = new JSONObject();
        result.set("code", 500);
        result.set("msg", null);
        result.set("content", null);
        for (String address : adminAddress()) {
            try {
                String content = HttpRequest.post(StrUtil.join("", address, api.getApi()))
                        .header(XXL_JOB_REQ_KEY, reqKey())
                        .timeout(10000)
                        .body(JSONUtil.toJsonStr(ObjectUtil.isEmpty(params) ? new HashMap<>(0) : params))
                        .execute()
                        .body();
                result = JSONUtil.parseObj(content);
                if (result.getInt("code", 500) == 200) {
                    break;
                }
            }catch (Exception e) {
                log.error(e.getMessage());
            }
        }
        return result;
    }

    @Override
    public String groupId() {
        JSONObject json = post(
                XxlApiEnum.GET_GROUP_ID,
                MapUtil.builder(new HashMap<String,Object>(0))
                        .put("appname", config().getExecutorAppName())
                        .build()
        );
        if (log.isDebugEnabled()) {
            log.info(json.toString());
        }
        return json.getStr("content", "");
    }

    @Override
    public String addJob(
            String jobName,
            String author,
            String email,
            String cron,
            Date cronDate,
            String handlerName,
            Class<? extends IJobHandler> handlerClazz,
            String param) {
        //基本参数格式化
        String jobGroup = groupId();
        jobName = (BaseUtil.isEmpty(jobName)?"系统注册" : BaseUtil.trim(jobName))
                + "("+DateUtil.formatDateTime(new Date())+")";
        author = BaseUtil.isEmpty(author) ? "系统" : BaseUtil.trim(author);
        String jobCron = BaseUtil.isEmpty(cron) ?
                DateUtil.format(cronDate == null ? new Date() : cronDate,"ss mm HH dd MM ? yyyy" ) : BaseUtil.trim(cron);
        Assert.notEmpty(BaseUtil.trim(handlerName), () -> new ApiException("202", "任务处理器名称不能为空"));
        //请求参数组装
        Map<String, Object> params = MapUtil.builder(new HashMap<String, Object>(0))
                .put("jobGroup", jobGroup)
                .put("jobDesc", jobName)
                .put("author", author)
                .put("alarmEmail", BaseUtil.trim(email))
                .put("scheduleType", "CRON")
                .put("scheduleConf", jobCron)
                .put("cronGen_display", jobCron)
                .put("glueType", "BEAN")
                .put("executorRouteStrategy", "CONSISTENT_HASH")
                .put("misfireStrategy", "FIRE_ONCE_NOW")
                .put("executorBlockStrategy", "SERIAL_EXECUTION")
                .put("executorTimeout", "0")
                .put("executorFailRetryCount", "3")
                .put("glueRemark", "BEAN模式")
                .put("executorParam", BaseUtil.trim(param))
                .put("executorHandler", BaseUtil.trim(handlerName))
                //非必填参数
                .put("schedule_conf_CRON", "")
                .put("schedule_conf_FIX_RATE", "")
                .put("schedule_conf_FIX_DELAY", "")
                .put("childJobId", "")
                .put("glueSource", "")
                .build();
        //调用接口实现任务注册
        JSONObject json = post(XxlApiEnum.ADD_JOB, params);
        if (log.isDebugEnabled()) {
            log.info(json.toString());
        }
        //创建注册handler
        if (json.getInt("code", 500) == 200 && handlerClazz != null) {
            try {
                IJobHandler object = SpringUtil.getBean(BaseUtil.trim(handlerName),handlerClazz);
                XxlJobExecutor.registJobHandler(BaseUtil.trim(handlerName), object);
            } catch (Exception e) {
                throw new ApiException("202", e.getMessage());
            }
        }
        return json.getStr("content", "");
    }

    @Override
    public boolean updateJob(
            String jobId,
            String jobName,
            String author,
            String email,
            String cron,
            Date cronDate,
            String handlerName,
            Class<? extends IJobHandler> handlerClazz,
            String param) {
        //基本参数格式化
        String jobGroup = groupId();
        Assert.notEmpty(BaseUtil.trim(jobId), () -> new ApiException("202", "任务ID不能为空"));
        author = BaseUtil.isEmpty(author) ? "系统" : BaseUtil.trim(author);
        jobName = (BaseUtil.isEmpty(jobName)?"系统注册" : BaseUtil.trim(jobName))
                + "("+DateUtil.formatDateTime(new Date())+")";
        String jobCron = BaseUtil.isEmpty(cron) ?
                DateUtil.format(cronDate == null ? new Date() : cronDate,"ss mm HH dd MM ? yyyy" ) : BaseUtil.trim(cron);
        Assert.notEmpty(BaseUtil.trim(handlerName), () -> new ApiException("202", "任务处理器名称不能为空"));
        //请求参数组装
        Map<String, Object> params = MapUtil.builder(new HashMap<String, Object>(0))
                .put("id", BaseUtil.trim(jobId))
                .put("jobGroup", jobGroup)
                .put("jobDesc", jobName)
                .put("author", author)
                .put("alarmEmail", BaseUtil.trim(email))
                .put("scheduleType", "CRON")
                .put("scheduleConf", jobCron)
                .put("cronGen_display", jobCron)
                .put("schedule_conf_CRON", jobCron)
                .put("executorHandler", BaseUtil.trim(handlerName))
                .put("executorParam", BaseUtil.trim(param))
                .put("executorRouteStrategy", "CONSISTENT_HASH")
                .put("misfireStrategy", "FIRE_ONCE_NOW")
                .put("executorBlockStrategy", "SERIAL_EXECUTION")
                .put("executorTimeout", "0")
                .put("executorFailRetryCount", "3")
                .put("schedule_conf_FIX_RATE", "")
                .put("schedule_conf_FIX_DELAY", "")
                .put("childJobId", "")
                .build();
        //调用接口实现任务注册
        JSONObject json = post(XxlApiEnum.UPDATE_JOB, params);
        if (log.isDebugEnabled()) {
            log.info(json.toString());
        }
        //创建注册handler
        if (json.getInt("code", 500) == 200) {
            if (handlerClazz != null) {
                try {
                    IJobHandler object = SpringUtil.getBean(BaseUtil.trim(handlerName),handlerClazz);
                    XxlJobExecutor.registJobHandler(BaseUtil.trim(handlerName), object);
                } catch (Exception e) {
                    throw new ApiException("202", e.getMessage());
                }
            }
            return true;
        }
        return false;
    }

    @Override
    public boolean pauseJob(String jobId) {
        Assert.notEmpty(BaseUtil.trim(jobId), () -> new ApiException("202", "任务ID不能为空"));
        JSONObject json = post(XxlApiEnum.PAUSE_JOB, MapUtil.builder(new HashMap<String, Object>(0))
                .put("id", BaseUtil.trim(jobId))
                .build());
        if (log.isDebugEnabled()) {
            log.info(json.toString());
        }
        return json.getInt("code", 500) == 200;
    }

    @Override
    public boolean startJob(String jobId) {
        Assert.notEmpty(BaseUtil.trim(jobId), () -> new ApiException("202", "任务ID不能为空"));
        JSONObject json = post(XxlApiEnum.START_JOB, MapUtil.builder(new HashMap<String, Object>(0))
                .put("id", BaseUtil.trim(jobId))
                .build());
        if (log.isDebugEnabled()) {
            log.info(json.toString());
        }
        return json.getInt("code", 500) == 200;
    }

    @Override
    public boolean removeJob(String jobId) {
        Assert.notEmpty(BaseUtil.trim(jobId), () -> new ApiException("202", "任务ID不能为空"));
        JSONObject json = post(XxlApiEnum.REMOVE_JOB, MapUtil.builder(new HashMap<String, Object>(0))
                .put("id", BaseUtil.trim(jobId))
                .build());
        if (log.isDebugEnabled()) {
            log.info(json.toString());
        }
        return json.getInt("code", 500) == 200;
    }

}
