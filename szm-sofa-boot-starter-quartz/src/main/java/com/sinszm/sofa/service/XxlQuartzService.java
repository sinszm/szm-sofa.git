package com.sinszm.sofa.service;

import com.sinszm.sofa.util.BaseUtil;
import com.xxl.job.core.handler.IJobHandler;

import java.util.Date;

/**
 * xxl任务调度服务
 *
 * @author admin
 * @date 2022/02/18
 */
public interface XxlQuartzService {

    /**
     * 分组ID
     *
     * @return {@link String}
     */
    String groupId();

    /**
     * 添加工作
     *
     * @param jobName      作业名
     * @param author       作者
     * @param email        电子邮件
     * @param cron         cron
     * @param cronDate     cron日期
     * @param handlerName  处理程序名称
     * @param handlerClazz 处理程序clazz
     * @param param        扩展参数
     * @return {@link String}
     */
    String addJob(String jobName,
                  String author,
                  String email,
                  String cron,
                  Date cronDate,
                  String handlerName,
                  Class<? extends IJobHandler> handlerClazz,
                  String param);

    /**
     * 更新工作
     *
     * @param jobId        工作id
     * @param jobName      作业名
     * @param author       作者
     * @param email        电子邮件
     * @param cron         cron
     * @param cronDate     cron日期
     * @param handlerName  处理程序名称
     * @param handlerClazz 处理程序clazz
     * @param param        参数
     * @return boolean
     */
    boolean updateJob(
            String jobId,
            String jobName,
            String author,
            String email,
            String cron,
            Date cronDate,
            String handlerName,
            Class<? extends IJobHandler> handlerClazz,
            String param);

    /**
     * 暂停工作
     *
     * @param jobId 工作id
     * @return boolean
     */
    boolean pauseJob(String jobId);

    /**
     * 开始工作
     *
     * @param jobId 工作id
     * @return boolean
     */
    boolean startJob(String jobId);

    /**
     * 删除工作
     *
     * @param jobId 工作id
     * @return boolean
     */
    boolean removeJob(String jobId);

    /**
     * 添加并开始工作
     *
     * @param jobName      作业名
     * @param author       作者
     * @param email        电子邮件
     * @param cron         cron
     * @param cronDate     cron日期
     * @param handlerName  处理程序名称
     * @param handlerClazz 处理程序clazz
     * @param param        参数
     * @return boolean
     */
    default boolean addAndStartJob(String jobName,
                                   String author,
                                   String email,
                                   String cron,
                                   Date cronDate,
                                   String handlerName,
                                   Class<? extends IJobHandler> handlerClazz,
                                   String param) {
        String jobId = addJob(jobName, author, email, cron, cronDate, handlerName, handlerClazz, param);
        if (BaseUtil.isEmpty(jobId)) {
            return false;
        }
        return startJob(jobId);
    }

    /**
     * 更新并开始工作
     *
     * @param jobId        工作id
     * @param jobName      作业名
     * @param author       作者
     * @param email        电子邮件
     * @param cron         cron
     * @param cronDate     cron日期
     * @param handlerName  处理程序名称
     * @param handlerClazz 处理程序clazz
     * @param param        参数
     * @return boolean
     */
    default boolean updateAndStartJob(String jobId,
                                      String jobName,
                                      String author,
                                      String email,
                                      String cron,
                                      Date cronDate,
                                      String handlerName,
                                      Class<? extends IJobHandler> handlerClazz,
                                      String param) {
        pauseJob(jobId);
        updateJob(jobId, jobName, author, email, cron, cronDate, handlerName, handlerClazz, param);
        return startJob(jobId);
    }

}
