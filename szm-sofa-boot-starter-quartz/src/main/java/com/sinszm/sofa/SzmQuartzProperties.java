package com.sinszm.sofa;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.ClassUtil;
import cn.hutool.core.util.ObjectUtil;
import com.sinszm.sofa.annotation.QJob;
import com.sinszm.sofa.exception.ApiException;
import com.sinszm.sofa.util.BaseUtil;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceInitializationMode;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.boot.jdbc.DataSourceInitializationMode.ALWAYS;

/**
 * 基础配置
 *
 * @author admin
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ConfigurationProperties("quartz")
public class SzmQuartzProperties {

    /**
     * 是否使用xxlJob任务方式，true是，false否
     */
    private boolean xxl;

    /**
     * xxlJob任务模式配置
     */
    private JobExecutorProperties xxlJob;

    /**
     * 指定数据源实例名称
     */
    @Deprecated
    private String dataSource;

    /**
     * 模式
     */
    @Deprecated
    private DataSourceInitializationMode mode;

    /**
     * 任务实例名
     */
    @Deprecated
    private String instanceName;

    /**
     * 基本包
     */
    @Deprecated
    private List<String> basePackages;

    /**
     * 基本包类
     */
    @Deprecated
    private List<Class<?>> basePackagesClasses;

    /**
     * 扫描任务是否启用，默认不启用初始化扫描任务
     */
    @Deprecated
    private boolean scanTask = false;

    /**
     * 检查所有
     *
     * @return {SzmQuartzProperties}
     */
    public SzmQuartzProperties checkAll() {
        if (!this.xxl) {
            Assert.notEmpty(BaseUtil.trim(this.dataSource), () -> new ApiException("202", "请先指定数据源实例名称"));
            this.dataSource = BaseUtil.trim(this.dataSource);
            this.mode = mode == null ? ALWAYS : this.mode;
            this.instanceName = BaseUtil.isEmpty(this.instanceName) ? "quartzScheduler" : this.instanceName;
        } else {
            Assert.notNull(this.xxlJob, () -> new ApiException("202", "请先配置任务调度中心"));
            Assert.notEmpty(BaseUtil.trim(this.xxlJob.adminAddress), () -> new ApiException("202", "请先配置任务调度中心地址"));
            Assert.notEmpty(BaseUtil.trim(this.xxlJob.accessToken), () -> new ApiException("202", "请先配置任务调度中心accessToken"));
            this.xxlJob.accessToken = BaseUtil.trim(this.xxlJob.accessToken);
            this.xxlJob.executorAppName = BaseUtil.trim(this.xxlJob.executorAppName);
            this.xxlJob.executorAddress = BaseUtil.trim(this.xxlJob.executorAddress);
            this.xxlJob.executorIp = BaseUtil.trim(this.xxlJob.executorIp);
            this.xxlJob.executorLogPath = BaseUtil.trim(this.xxlJob.executorLogPath);
        }
        return this;
    }

    /**
     * 获取所有具备QJob注解的类
     *
     * @return 类加载
     */
    public SzmQuartzProperties jobClasses() {
        List<Class<?>> classes = new LinkedList<>();
        if (ObjectUtil.isNotEmpty(this.basePackages)) {
            for (String basePackage : basePackages) {
                classes.addAll(ClassUtil.scanPackageByAnnotation(basePackage, QJob.class));
            }
        }
        if (ObjectUtil.isNotEmpty(basePackagesClasses)) {
            classes.addAll(basePackagesClasses);
        }
        this.basePackagesClasses = classes.stream().distinct().collect(Collectors.toList());
        return this;
    }

    @Data
    public static class JobExecutorProperties {

        /**
         * 调度中心部署跟地址 [选填]：如调度中心集群部署存在多个地址则用逗号分隔。执行器将会使用该地址进行"执行器心跳注册"和"任务结果回调"；为空则关闭自动注册；
         */
        private String adminAddress;

        /**
         * 执行器通讯TOKEN [选填]：非空时启用；
         */
        private String accessToken;

        /**
         * 执行器AppName [选填]：执行器心跳注册分组依据；为空则关闭自动注册
         */
        private String executorAppName;

        /**
         * 执行器注册 [选填]：优先使用该配置作为注册地址，为空时使用内嵌服务 ”IP:PORT“ 作为注册地址。从而更灵活的支持容器类型执行器动态IP和动态映射端口问题。
         */
        private String executorAddress;

        /**
         * 执行器IP [选填]：默认为空表示自动获取IP，多网卡时可手动设置指定IP，该IP不会绑定Host仅作为通讯实用；地址信息用于 "执行器注册" 和 "调度中心请求并触发任务"；
         */
        private String executorIp;

        /**
         * 执行器端口号 [选填]：小于等于0则自动获取；默认端口为9999，单机部署多个执行器时，注意要配置不同执行器端口；
         */
        private int executorPort = 9999;

        /**
         * 执行器运行日志文件存储磁盘路径 [选填] ：需要对该路径拥有读写权限；为空则使用默认路径；
         */
        private String executorLogPath;

        /**
         * 执行器日志文件保存天数 [选填] ： 过期日志自动清理, 限制值大于等于3时生效; 否则, 如-1, 关闭自动清理功能；
         */
        private int executorLogRetentionDays = 15;

    }

}
