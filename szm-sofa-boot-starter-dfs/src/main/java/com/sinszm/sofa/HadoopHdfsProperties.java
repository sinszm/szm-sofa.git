package com.sinszm.sofa;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 文件存储服务器配置
 * <p>
 *     hadoop HDFS
 * </p>
 * @author sinszm
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ConfigurationProperties("dfs.hdfs")
public class HadoopHdfsProperties {

    /**
     * 默认fs
     */
    private String defaultFs;

    /**
     * 临时缓存目录
     */
    private String hadoopHomeDir;

    /**
     * 存储桶
     */
    private String bucket;

    /**
     * 缓存启用
     */
    private boolean cacheEnable = true;

}
