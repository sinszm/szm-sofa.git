package com.sinszm.sofa;


import cn.hutool.core.util.ObjectUtil;
import com.sinszm.sofa.util.BaseUtil;
import lombok.extern.slf4j.Slf4j;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.codec.SerializationCodec;
import org.redisson.config.Config;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

import javax.annotation.Resource;

/**
 * 配置加载中心
 * @author sinszm
 */
@Slf4j
public class SofaBootStarterJedisConfiguration {

    @Resource
    private RedisProperties redisProperties;

    @Primary
    @Bean
    public RedissonClient redissonClient() {
        Config config = new Config();
        config.setCodec(new SerializationCodec());
        if (ObjectUtil.isNotNull(redisProperties.getCluster()) && ObjectUtil.isNotEmpty(redisProperties.getCluster().getNodes())) {
            config.useClusterServers()
                    .setMasterConnectionPoolSize(redisProperties.getLettuce().getPool().getMaxActive())
                    .setSlaveConnectionPoolSize(redisProperties.getLettuce().getPool().getMaxActive())
                    .setSubscriptionConnectionPoolSize(redisProperties.getLettuce().getPool().getMaxActive())
                    .setMasterConnectionMinimumIdleSize(redisProperties.getLettuce().getPool().getMinIdle())
                    .setSlaveConnectionMinimumIdleSize(redisProperties.getLettuce().getPool().getMinIdle())
                    .setSubscriptionConnectionMinimumIdleSize(redisProperties.getLettuce().getPool().getMinIdle())
                    .setConnectTimeout((int) redisProperties.getTimeout().toMillis())
                    .setIdleConnectionTimeout((int) redisProperties.getTimeout().toMillis())
                    .setKeepAlive(true)
                    .setTimeout((int) redisProperties.getTimeout().toMillis())
                    //集群模式
                    .setPassword(redisProperties.getPassword())
                    .setNodeAddresses(redisProperties.getCluster().getNodes());
            log.info("Redisson Cluster Created!");
            return Redisson.create(config);
        }
        if (ObjectUtil.isNotNull(redisProperties.getSentinel()) && ObjectUtil.isNotEmpty(redisProperties.getSentinel().getNodes())) {
            config.useSentinelServers()
                    .setMasterConnectionPoolSize(redisProperties.getLettuce().getPool().getMaxActive())
                    .setSlaveConnectionPoolSize(redisProperties.getLettuce().getPool().getMaxActive())
                    .setSubscriptionConnectionPoolSize(redisProperties.getLettuce().getPool().getMaxActive())
                    .setMasterConnectionMinimumIdleSize(redisProperties.getLettuce().getPool().getMinIdle())
                    .setSlaveConnectionMinimumIdleSize(redisProperties.getLettuce().getPool().getMinIdle())
                    .setSubscriptionConnectionMinimumIdleSize(redisProperties.getLettuce().getPool().getMinIdle())
                    .setConnectTimeout((int) redisProperties.getTimeout().toMillis())
                    .setIdleConnectionTimeout((int) redisProperties.getTimeout().toMillis())
                    .setKeepAlive(true)
                    .setTimeout((int) redisProperties.getTimeout().toMillis())
                    //哨兵模式
                    .setMasterName(redisProperties.getSentinel().getMaster())
                    .setPassword(redisProperties.getPassword())
                    .setSentinelPassword(redisProperties.getSentinel().getPassword())
                    .setSentinelAddresses(redisProperties.getSentinel().getNodes());
            log.info("Redisson Sentinel Created!");
            return Redisson.create(config);
        }
        config.useSingleServer()
                .setConnectionPoolSize(redisProperties.getLettuce().getPool().getMaxActive())
                .setSubscriptionConnectionPoolSize(redisProperties.getLettuce().getPool().getMaxActive())
                .setConnectionMinimumIdleSize(redisProperties.getLettuce().getPool().getMinIdle())
                .setSubscriptionConnectionMinimumIdleSize(redisProperties.getLettuce().getPool().getMinIdle())
                .setConnectTimeout((int) redisProperties.getTimeout().toMillis())
                .setIdleConnectionTimeout((int) redisProperties.getTimeout().toMillis())
                .setKeepAlive(true)
                .setTimeout((int) redisProperties.getTimeout().toMillis())
                //单节点模式
                .setDatabase(redisProperties.getDatabase())
                .setAddress(BaseUtil.isEmpty(redisProperties.getUrl()) ? "redis://"+redisProperties.getHost()+":"+redisProperties.getPort() : redisProperties.getUrl())
                .setPassword(redisProperties.getPassword());
        log.info("Redisson Single Created!");
        return Redisson.create(config);
    }

}
