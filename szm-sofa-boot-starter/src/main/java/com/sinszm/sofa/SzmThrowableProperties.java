package com.sinszm.sofa;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 是否启用Throwable信息的输出配置
 * @author sinszm
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ConfigurationProperties(prefix = "throw.point")
public class SzmThrowableProperties {
    /**
     * 是否启用Throwable信息的输出
     */
    private boolean enabled = false;

}
