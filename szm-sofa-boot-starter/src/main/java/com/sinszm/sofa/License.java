package com.sinszm.sofa;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.lang.id.NanoId;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.net.NetUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.json.JSONUtil;
import cn.hutool.system.SystemUtil;
import com.sinszm.sofa.exception.ApiException;
import com.sinszm.sofa.util.BaseUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 许可证
 *
 * @author admin
 * @date 2022/07/14
 */
public class License {

    private static final Map<String, String> LICENSE_DAY = new ConcurrentHashMap<>(0);

    private static final String DEFAULT_KEY = "4c0f3db21eafac0eeb2c5d33634368f6";

    /**
     * 根据域名获取IP判断网络连接性
     *
     * @param hostname 主机名
     * @return boolean
     */
    private static boolean isConnected(String hostname) {
        if (BaseUtil.isEmpty(hostname)) {
            return false;
        }
        return !BaseUtil.trim(hostname).equalsIgnoreCase(NetUtil.getIpByHost(BaseUtil.trim(hostname)));
    }

    /**
     * 根据几个常用关键性域名来判断网络连接性
     *
     * @return boolean
     */
    private static boolean isConnected() {
        List<String> list = Arrays.asList(
                "mirrors-1251221235.cos.ap-chongqing.myqcloud.com",
                "api.weixin.qq.com",
                "open.weixin.qq.com",
                "openapi.alipay.com",
                "gitee.com"
        );
        for (String hostname : list) {
            if (isConnected(hostname)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 验证许可证（内网运行不限制）
     *
     * @param name     厂商英文名称
     * @param tag      文件标识
     * @param eTag     文件内容版本标识
     * @param password 密码
     */
    public static void validateLicense(String name, String tag, String eTag, String password) {
        Date currentDate = DateUtil.beginOfDay(DateUtil.date());
        //验证默认KEY
        String argStr = SecureUtil.md5()
                .digestHex(
                        StrUtil.join(
                                "",
                                "000",
                                BaseUtil.trim(name),
                                BaseUtil.trim(tag),
                                BaseUtil.trim(eTag),
                                BaseUtil.trim(password)
                        )
                );
        if (DEFAULT_KEY.equals(argStr)) {
            return;
        }
        if (isConnected() && !LICENSE_DAY.containsKey(DateUtil.formatDate(currentDate))) {
            if (BaseUtil.isEmpty(name)
                    || BaseUtil.isEmpty(tag)
                    || BaseUtil.isEmpty(eTag)
                    || BaseUtil.isEmpty(password)) {
                throw new ApiException("-1", "授权许可配置参数缺失，请配置完整重试!");
            }
            try {
                HttpResponse response = HttpRequest.get(
                        StrUtil.join(
                                "/",
                                "https://mirrors-1251221235.cos.ap-chongqing.myqcloud.com/license",
                                BaseUtil.trim(name),
                                BaseUtil.trim(tag))
                ).timeout(6000).execute();
                String respTag = response.headers()
                        .getOrDefault("ETag", new ArrayList<>())
                        .stream()
                        .findFirst()
                        .orElse("")
                        .replace("\"", "");
                if (!respTag.equals(BaseUtil.trim(eTag))) {
                    throw new ApiException("-1", "授权许可内容标识版本不匹配，请正确配置重试!");
                }
                String code = SecureUtil.hmacMd5(BaseUtil.trim(password)).digestHex(
                        StrUtil.join("", BaseUtil.trim(name),BaseUtil.trim(tag))
                );
                String content = BaseUtil.trim(response.body()).replace(";" + code, "");
                if (content.length() != 10) {
                    throw new ApiException("-1", "授权许可密码不匹配，请配置正确重试!");
                }
                Date defaultDate = DateUtil.parseDate("1970-01-01");
                Date date = DateUtil.parseDate(content);
                boolean isValid = DateUtil.isIn(
                        currentDate,
                        DateUtil.beginOfDay(defaultDate),
                        DateUtil.beginOfDay(date)
                );
                if (!isValid) {
                    throw new ApiException("-1", "授权许可已过期，请联系框架管理员购买授权!");
                }
                LICENSE_DAY.put(DateUtil.formatDate(currentDate), DateUtil.now());
            } catch (Exception e) {
                if (e instanceof ApiException) {
                    throw (ApiException)e;
                }
                throw new ApiException("-1", "授权许可验证异常，请检查网络或配置重试!");
            }
        }
    }

    /**
     * 生成许可证
     *
     * @param name           厂商英文名称
     * @param expirationDate 生效日期
     * @return {@link String}
     */
    public static String generateLicense(String name, String expirationDate) throws FileNotFoundException {
        Map<String, String> map = new TreeMap<>();
        if (BaseUtil.isEmpty(name) || BaseUtil.isEmpty(expirationDate)) {
            return JSONUtil.toJsonStr(map);
        }
        String namespace = BaseUtil.trim(name);
        String tag = NanoId.randomNanoId(32);
        String password = NanoId.randomNanoId();
        String date = BaseUtil.trim(expirationDate);
        String code = SecureUtil.hmacMd5(BaseUtil.trim(password)).digestHex(
                StrUtil.join("", BaseUtil.trim(namespace),BaseUtil.trim(tag))
        );
        String content = StrUtil.join(";", date, code);
        //创建授权许可证文件
        File file = FileUtil.touch(
                StrUtil.join(File.separator, SystemUtil.getUserInfo().getCurrentDir(),"tmp", namespace, tag)
        );
        IoUtil.writeUtf8(new FileOutputStream(file), true, content);
        return JSONUtil.toJsonStr(
                MapUtil.builder(map)
                        .put("name", namespace)
                        .put("tag", tag)
                        .put("eTag", "需要框架管理员上传到验证服务器之后提供!")
                        .put("password", password)
                        .build()
        );
    }

}
