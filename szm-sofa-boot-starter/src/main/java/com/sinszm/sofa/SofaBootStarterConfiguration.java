package com.sinszm.sofa;


import cn.hutool.extra.spring.EnableSpringUtil;
import com.sinszm.sofa.annotation.EnableValidate;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;

/**
 * 配置加载中心
 * @author sinszm
 */
@EnableValidate
@EnableSpringUtil
@ComponentScans({@ComponentScan("com.sinszm.*")})
@PropertySource("classpath:settings.properties")
@EnableConfigurationProperties({SzmLogProperties.class, SzmThrowableProperties.class, SzmLicenseProperties.class})
public class SofaBootStarterConfiguration implements WebMvcConfigurer {

    @Resource
    private LicenseAdapter licenseAdapter;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(licenseAdapter).addPathPatterns("/**");
    }

}
