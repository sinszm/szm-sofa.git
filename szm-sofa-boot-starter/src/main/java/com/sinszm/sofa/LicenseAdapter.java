package com.sinszm.sofa;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 许可证适配器
 *
 * @author admin
 * @date 2022/07/14
 */
@Component
public class LicenseAdapter extends HandlerInterceptorAdapter {

    @Resource
    private SzmLicenseProperties szmLicenseProperties;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        License.validateLicense(
                szmLicenseProperties.getName(),
                szmLicenseProperties.getTag(),
                szmLicenseProperties.getETag(),
                szmLicenseProperties.getPassword()
        );
        return super.preHandle(request, response, handler);
    }

}
