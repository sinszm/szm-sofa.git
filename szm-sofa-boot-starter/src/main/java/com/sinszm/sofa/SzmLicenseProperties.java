package com.sinszm.sofa;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 许可授权属性
 *
 * @author sinszm
 * @date 2022/07/14
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ConfigurationProperties(prefix = "license")
public class SzmLicenseProperties {

    /**
     * 厂商英文名称标识
     */
    private String name;

    /**
     * 授权文件名称标识
     */
    private String tag;

    /**
     * 授权文件版本标识
     */
    private String eTag;

    /**
     * 密码
     */
    private String password;

}
