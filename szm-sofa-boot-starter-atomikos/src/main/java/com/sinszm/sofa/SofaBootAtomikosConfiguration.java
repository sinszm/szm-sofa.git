package com.sinszm.sofa;

import com.alibaba.druid.pool.xa.DruidXADataSource;
import com.atomikos.icatch.config.UserTransactionServiceImp;
import com.atomikos.icatch.jta.UserTransactionImp;
import com.atomikos.icatch.jta.UserTransactionManager;
import com.atomikos.jdbc.AtomikosDataSourceBean;
import com.microsoft.sqlserver.jdbc.SQLServerXADataSource;
import com.mysql.cj.jdbc.MysqlXADataSource;
import com.sinszm.sofa.exception.ApiException;
import com.sinszm.sofa.response.StatusCode;
import com.sinszm.sofa.support.DataSourceUtil;
import com.sinszm.sofa.util.BaseUtil;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.transaction.TransactionManagerCustomizers;
import org.springframework.boot.autoconfigure.transaction.jta.JtaProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.jta.atomikos.AtomikosProperties;
import org.springframework.boot.system.ApplicationHome;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Primary;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.jta.JtaTransactionManager;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import java.io.File;
import java.sql.SQLException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * atomikos配置
 *
 * @author admin
 */
@EnableConfigurationProperties({DataSourceProperties.class, AtomikosProperties.class, JtaProperties.class})
public class SofaBootAtomikosConfiguration {

    @Resource
    private DataSourceProperties dataSourceProperties;

    @Bean
    public Map<String, AtomikosDataSourceBean> dataSourceBeanMap() {
        return Optional.ofNullable(dataSourceProperties.getDb())
                .orElse(new LinkedHashMap<>(0))
                .entrySet()
                .stream()
                .map(entry -> {
                    DataSourceAttribute db = entry.getValue();
                    String uniqueResourceName = entry.getKey();
                    if (db == null || BaseUtil.isEmpty(uniqueResourceName)) {
                        return null;
                    }
                    //判断数据源类型特殊逻辑处理
                    AtomikosDataSourceBean ads = new AtomikosDataSourceBean();
                    final String mssqlDriver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
                    //SqlServer需要特殊处理
                    if (mssqlDriver.equalsIgnoreCase(db.getDriverClassName())) {
                        SQLServerXADataSource mssql = new SQLServerXADataSource();
                        mssql.setURL(db.getUrl());
                        mssql.setUser(db.getUsername());
                        mssql.setPassword(db.getPassword());
                        ads.setXaDataSource(mssql);
                    } if(BaseUtil.trim(db.getDriverClassName()).contains("mysql")) {
                        MysqlXADataSource dds = new MysqlXADataSource();
                        dds.setUrl(db.getUrl());
                        dds.setUser(db.getUsername());
                        dds.setPassword(db.getPassword());
                        try {
                            dds.setPinGlobalTxToPhysicalConnection(true);
                        } catch (SQLException ignored) { }
                        ads.setXaDataSource(dds);
                    } else {
                        DruidXADataSource dds = new DruidXADataSource();
                        dds.setUrl(db.getUrl());
                        dds.setUsername(db.getUsername());
                        dds.setPassword(db.getPassword());
                        dds.setDriverClassName(db.getDriverClassName());
                        dds.setMaxActive(db.getMaxPoolSize());
                        dds.setMaxWait(db.getMaxLifetime());
                        dds.setMinIdle(db.getMinPoolSize());
                        if (!BaseUtil.isEmpty(db.getTestQuerySql())) {
                            dds.setValidationQuery(db.getTestQuerySql());
                        } else {
                            if (db.getDriverClassName().contains("mysql")) {
                                dds.setValidationQuery("select 1");
                            }
                            if (db.getDriverClassName().contains("oracle")) {
                                dds.setValidationQuery("select 1 from dual");
                            }
                        }
                        dds.setTestOnBorrow(true);
                        dds.setTestOnReturn(false);
                        dds.setTestWhileIdle(false);
                        dds.setKeepAlive(true);
                        ads.setXaDataSource(dds);
                    }
                    ads.setUniqueResourceName(uniqueResourceName);
                    ads.setMinPoolSize(db.getMinPoolSize());
                    ads.setMaxPoolSize(db.getMaxPoolSize());
                    ads.setMaxLifetime(db.getMaxLifetime());
                    ads.setBorrowConnectionTimeout(db.getBorrowConnectionTimeout());
                    try {
                        ads.setLoginTimeout(db.getLoginTimeout());
                    } catch (SQLException e) {
                        return null;
                    }
                    ads.setMaintenanceInterval(db.getMaintenanceInterval());
                    ads.setMaxIdleTime(db.getMaxIdleTime());
                    if (!BaseUtil.isEmpty(db.getTestQuerySql())) {
                        ads.setTestQuery(db.getTestQuerySql());
                    } else {
                        if (db.getDriverClassName().contains("mysql")) {
                            ads.setTestQuery("select 1");
                        }
                        if (db.getDriverClassName().contains("oracle")) {
                            ads.setTestQuery("select 1 from dual");
                        }
                    }
                    return ads;
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toMap(AtomikosDataSourceBean::getUniqueResourceName, Function.identity(),(a,b) -> a,LinkedHashMap::new));
    }

    @Bean(initMethod = "init", destroyMethod = "shutdownWait")
    @ConditionalOnMissingBean(PlatformTransactionManager.class)
    UserTransactionServiceImp userTransactionService(
            final AtomikosProperties atomikosProperties,
            final JtaProperties jtaProperties) {
        Properties properties = new Properties();
        if (StringUtils.hasText(jtaProperties.getTransactionManagerId())) {
            properties.setProperty("com.atomikos.icatch.tm_unique_name", jtaProperties.getTransactionManagerId());
        }
        properties.setProperty("com.atomikos.icatch.log_base_dir", getLogBaseDir(jtaProperties));
        properties.putAll(atomikosProperties.asProperties());
        return new UserTransactionServiceImp(properties);
    }

    private String getLogBaseDir(JtaProperties jtaProperties) {
        if (StringUtils.hasLength(jtaProperties.getLogDir())) {
            return jtaProperties.getLogDir();
        }
        File home = new ApplicationHome().getDir();
        return new File(home, "transaction-logs").getAbsolutePath();
    }

    @Bean(name = "userTransaction")
    @ConditionalOnMissingBean(PlatformTransactionManager.class)
    public UserTransaction userTransaction() throws SystemException {
        UserTransactionImp userTransactionImp = new UserTransactionImp();
        userTransactionImp.setTransactionTimeout(60);
        return userTransactionImp;
    }

    @Bean(name = "atomikosTransactionManager",initMethod = "init", destroyMethod = "close")
    @DependsOn({"userTransactionService"})
    @ConditionalOnMissingBean(PlatformTransactionManager.class)
    public UserTransactionManager atomikosTransactionManager() {
        UserTransactionManager userTransactionManager = new UserTransactionManager();
        userTransactionManager.setForceShutdown(true);
        userTransactionManager.setStartupTransactionService(true);
        try {
            userTransactionManager.setTransactionTimeout(60);
        } catch (SystemException e) {
            throw new ApiException(StatusCode.FAIL.getCode(), "事务设置超时时间异常.");
        }
        return userTransactionManager;
    }

    @Bean(name = "jtaTransactionManager")
    @DependsOn({ "userTransaction", "atomikosTransactionManager" })
    @ConditionalOnMissingBean(PlatformTransactionManager.class)
    public PlatformTransactionManager jtaTransactionManager(
            @Qualifier("userTransaction") final UserTransaction userTransaction,
            @Qualifier("atomikosTransactionManager") final UserTransactionManager atomikosTransactionManager,
            ObjectProvider<TransactionManagerCustomizers> transactionManagerCustomizers) {
        JtaTransactionManager jta = new JtaTransactionManager(userTransaction, atomikosTransactionManager);
        jta.setAllowCustomIsolationLevels(true);
        jta.setDefaultTimeout(60);
        transactionManagerCustomizers.ifAvailable((customizers) -> customizers.customize(jta));
        return jta;
    }

    @Bean
    @DependsOn("dataSourceBeanMap")
    public DataSourceUtil dataSourceUtil() {
        return new DataSourceUtil();
    }

    @Configuration
    public static class DefaultSqlSessionConfiguration extends AbstractSqlSessionConfiguration {

        @Autowired(required = false)
        public DefaultSqlSessionConfiguration(DataSourceUtil dataSourceUtil) {
            super(dataSourceUtil);
        }

        @Bean
        @Primary
        @Override
        public AtomikosDataSourceBean dataSource() {
            return super.dataSourceUtil.getDefaultDataSource();
        }

        @Bean
        @DependsOn("dataSource")
        @Override
        public SqlSessionFactory sqlSessionFactory() throws Exception {
            return super.getSqlSessionFactory("dataSource");
        }

        @Bean
        @DependsOn("sqlSessionFactory")
        @Override
        public SqlSessionTemplate sqlSessionTemplate() {
            return super.getSqlSessionTemplate("sqlSessionFactory");
        }

    }

}
