package com.sinszm.sofa.util;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ReflectUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.plugins.pagination.PageDTO;

import java.lang.reflect.Field;
import java.util.stream.Collectors;

/**
 * 分页数据格式化工具
 *
 * @author admin
 */
public class PageFormatUtil {

    /**
     * 计算指数
     *
     * @param response 响应
     * @param index    指数
     * @return long
     */
    private static <T> long calculateIndex(Page<T> response, Integer index) {
        int currentIndex = (index == null ? 0 : index) + 1;
        return response.getCurrent() <= 1 ? currentIndex : currentIndex + (response.getCurrent() - 1) * response.getSize();
    }

    /**
     * 格式
     *
     * @param list           列表
     * @param clazz          clazz
     * @param indexFieldName 序号字段名
     * @return 分页数据
     */
    public static <E,T> PageDTO<T> format(Page<E> list, Class<T> clazz, String indexFieldName) {
        PageDTO<T> result = new PageDTO<>(list.getCurrent(), list.getSize(), list.getTotal(),list.searchCount());
        result.setOrders(list.orders());
        result.setOptimizeCountSql(list.optimizeCountSql());
        result.setRecords(
                list.getRecords().stream()
                        .map(l -> {
                            T r = ReflectUtil.newInstance(clazz);
                            BeanUtil.copyProperties(l, r);
                            return r;
                        })
                        .peek(BaseUtil.consumerWithIndex((data, index) -> {
                            Field field = ReflectUtil.getField(clazz, indexFieldName);
                            if (field != null) {
                                field.setAccessible(true);
                                ReflectUtil.setFieldValue(data, field, calculateIndex(result, index));
                            }
                        }))
                        .collect(Collectors.toList())
        );
        return result;
    }

}
