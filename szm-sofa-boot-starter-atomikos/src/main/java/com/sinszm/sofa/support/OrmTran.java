package com.sinszm.sofa.support;

import org.springframework.transaction.annotation.Transactional;

import java.lang.annotation.*;

/**
 * ORM事务封装注解
 *
 * @author chenjianbo
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
@Transactional(rollbackFor = Exception.class)
public @interface OrmTran {
}
