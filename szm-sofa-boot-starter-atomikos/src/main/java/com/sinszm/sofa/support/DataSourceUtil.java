package com.sinszm.sofa.support;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.map.MapUtil;
import com.atomikos.jdbc.AtomikosDataSourceBean;
import com.baomidou.mybatisplus.autoconfigure.SpringBootVFS;
import com.baomidou.mybatisplus.core.MybatisConfiguration;
import com.baomidou.mybatisplus.core.config.GlobalConfig;
import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.baomidou.mybatisplus.core.incrementer.IKeyGenerator;
import com.baomidou.mybatisplus.core.incrementer.IdentifierGenerator;
import com.baomidou.mybatisplus.core.injector.ISqlInjector;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.BlockAttackInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.OptimisticLockerInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;
import com.sinszm.sofa.DataSourceAttribute;
import com.sinszm.sofa.DataSourceProperties;
import com.sinszm.sofa.exception.ApiException;
import com.sinszm.sofa.util.BaseUtil;
import lombok.Getter;
import org.apache.ibatis.logging.stdout.StdOutImpl;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.type.JdbcType;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import static com.sinszm.sofa.response.StatusCode.SQL_ERROR;

/**
 * 数据源工具
 *
 * @author admin
 */
@Getter
public class DataSourceUtil {

    @Resource
    private Map<String, AtomikosDataSourceBean> dataSourceBeanMap;

    @Resource
    private DataSourceProperties dataSourceProperties;

    @Resource
    private ApplicationContext applicationContext;

    private static final String ORACLE_KEYWORDS = "oracle";

    private ApiException error(String message) {
        ApiException e = new ApiException(SQL_ERROR);
        if (!BaseUtil.isEmpty(message)) {
            e.setMessage(BaseUtil.trim(message));
        }
        return e;
    }

    /**
     * 获取默认数据源
     *
     * @return {@link DataSource}
     */
    public AtomikosDataSourceBean getDefaultDataSource() {
        for (Map.Entry<String, AtomikosDataSourceBean> entry : dataSourceBeanMap.entrySet()) {
            return entry.getValue();
        }
        throw error("数据源不存在");
    }

    /**
     * 获取数据来源
     *
     * @param sourceName 源名称
     * @return {@link DataSource}
     */
    public AtomikosDataSourceBean getDataSource(String sourceName) {
        AtomikosDataSourceBean dataSource = MapUtil.get(dataSourceBeanMap, BaseUtil.trim(sourceName), AtomikosDataSourceBean.class, null);
        Assert.notNull(dataSource, () -> error("数据源不存在"));
        return dataSource;
    }

    /**
     * sql会话工厂
     *
     * @param sourceName 源名称
     * @param dataSource 数据源
     * @return {@link SqlSessionFactory}
     */
    public SqlSessionFactory getFactory(String sourceName, DataSource dataSource) throws Exception {
        Map<String, DataSourceAttribute> dbMap = dataSourceProperties.getDb();
        Assert.notEmpty(dbMap, () -> error("配置不存在"));
        DataSourceAttribute db = MapUtil.get(dbMap, BaseUtil.trim(sourceName), DataSourceAttribute.class, null);
        Assert.notNull(db, () -> error("数据源配置不存在"));
        Map<String,Object> properties = BeanUtil.beanToMap(dataSourceProperties);
        //全局配置
        GlobalConfig global = new GlobalConfig();
        boolean enableLogicField = MapUtil.getBool(properties, "enableLogicDelField", false);
        if (enableLogicField) {
            GlobalConfig.DbConfig dbConfig = new GlobalConfig.DbConfig();
            dbConfig.setLogicDeleteField(MapUtil.getStr(properties, "dbConfigLogicDeleteField", "del"));
            dbConfig.setLogicDeleteValue(MapUtil.getStr(properties, "dbConfigLogicDeleteValue", "1"));
            dbConfig.setLogicNotDeleteValue(MapUtil.getStr(properties, "dbConfigLogicNotDeleteValue", "0"));
            global.setDbConfig(dbConfig);
        }
        // TODO 注入填充器
        this.getBeanThen(MetaObjectHandler.class, global::setMetaObjectHandler);
        // TODO 注入主键生成器
        this.getBeansThen(IKeyGenerator.class, i -> global.getDbConfig().setKeyGenerators(i));
        // TODO 注入sql注入器
        this.getBeanThen(ISqlInjector.class, global::setSqlInjector);
        // TODO 注入ID生成器
        this.getBeanThen(IdentifierGenerator.class, global::setIdentifierGenerator);
        //分页拦截器
        PaginationInnerInterceptor paginationInnerInterceptor = new PaginationInnerInterceptor();
        paginationInnerInterceptor.setOverflow(MapUtil.getBool(properties, "pageOverflow", false));
        paginationInnerInterceptor.setMaxLimit(MapUtil.getLong(properties, "pageMaxLimit", 1000L));
        //插件绑定
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(paginationInnerInterceptor);
        interceptor.addInnerInterceptor(new OptimisticLockerInnerInterceptor());
        interceptor.addInnerInterceptor(new BlockAttackInnerInterceptor());
        //Mybatis配置
        MybatisConfiguration config = new MybatisConfiguration();
        config.setMapUnderscoreToCamelCase(MapUtil.getBool(properties, "mybatisMapUnderscoreToCamelCase", true));
        config.setDefaultStatementTimeout(MapUtil.getInt(properties, "mybatisDefaultStatementTimeout", 30));
        config.setDefaultFetchSize(MapUtil.getInt(properties, "mybatisDefaultFetchSize", 100));
        config.setCacheEnabled(MapUtil.getBool(properties, "mybatisCacheEnabled", false));
        config.setVfsImpl(dataSourceProperties.getMybatisVfsImpl() == null ? SpringBootVFS.class : dataSourceProperties.getMybatisVfsImpl());
        config.setLogImpl(dataSourceProperties.getMybatisLogImpl() == null ? StdOutImpl.class : dataSourceProperties.getMybatisLogImpl());
        config.setAggressiveLazyLoading(MapUtil.getBool(properties, "mybatisAggressiveLazyLoading", false));
        config.setUseGeneratedShortKey(MapUtil.getBool(properties, "mybatisUseGeneratedShortKey", true));
        config.addInterceptor(interceptor);
        if (BaseUtil.trim(db.getDriverClassName()).toLowerCase().contains(ORACLE_KEYWORDS)) {
            config.setJdbcTypeForNull(JdbcType.NULL);
        }
        //工厂定义
        MybatisSqlSessionFactoryBean bean = new MybatisSqlSessionFactoryBean();
        bean.setGlobalConfig(global);
        bean.setConfiguration(config);
        bean.setMapperLocations(
                new PathMatchingResourcePatternResolver()
                        .getResources(BaseUtil.trim(db.getMapperLocations()))
        );
        bean.setDataSource(dataSource);
        //自定义枚举包支持
        if (StringUtils.hasLength(db.getTypeEnumsPackage())) {
            bean.setTypeEnumsPackage(BaseUtil.trim(db.getTypeEnumsPackage()));
        }
        return bean.getObject();
    }

    /**
     * sql会话模板
     *
     * @param sqlSessionFactory sql会话工厂
     * @return {@link SqlSessionTemplate}
     */
    public SqlSessionTemplate getTemplate(SqlSessionFactory sqlSessionFactory) {
        return new SqlSessionTemplate(sqlSessionFactory);
    }

    /**
     * 检查spring容器里是否有对应的bean,有则进行消费
     *
     * @param clazz    class
     * @param consumer 消费
     * @param <T>      泛型
     */
    private <T> void getBeanThen(Class<T> clazz, Consumer<T> consumer) {
        if (this.applicationContext.getBeanNamesForType(clazz, false, false).length > 0) {
            consumer.accept(this.applicationContext.getBean(clazz));
        }
    }

    /**
     * 检查spring容器里是否有对应的bean,有则进行消费
     *
     * @param clazz    class
     * @param consumer 消费
     * @param <T>      泛型
     */
    private <T> void getBeansThen(Class<T> clazz, Consumer<List<T>> consumer) {
        if (this.applicationContext.getBeanNamesForType(clazz, false, false).length > 0) {
            final Map<String, T> beansOfType = this.applicationContext.getBeansOfType(clazz);
            List<T> clazzList = new ArrayList<>();
            beansOfType.forEach((k, v) -> clazzList.add(v));
            consumer.accept(clazzList);
        }
    }

}
