package com.sinszm.sofa;

import cn.hutool.extra.spring.SpringUtil;
import com.atomikos.jdbc.AtomikosDataSourceBean;
import com.sinszm.sofa.support.DataSourceUtil;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.context.annotation.DependsOn;

import javax.validation.constraints.NotEmpty;

/**
 * 抽象的sql会话配置
 *
 * @author admin
 */
@DependsOn("dataSourceUtil")
public abstract class AbstractSqlSessionConfiguration {

    protected final DataSourceUtil dataSourceUtil;

    public AbstractSqlSessionConfiguration(DataSourceUtil dataSourceUtil) {
        this.dataSourceUtil = dataSourceUtil;
    }

    /**
     * 获得sql会话工厂
     *
     * @param sourceName 源名称
     * @return {@link SqlSessionFactory}
     * @throws Exception 异常
     */
    protected SqlSessionFactory getSqlSessionFactory(@NotEmpty String sourceName) throws Exception {
        AtomikosDataSourceBean dataSource = SpringUtil.getBean(sourceName, AtomikosDataSourceBean.class);
        return dataSourceUtil.getFactory(dataSource.getUniqueResourceName(), dataSource);
    }

    /**
     * 获得sql会话模板
     *
     * @param factoryName 工厂的名字
     * @return {@link SqlSessionTemplate}
     */
    protected SqlSessionTemplate getSqlSessionTemplate(@NotEmpty String factoryName) {
        SqlSessionFactory sqlSessionFactory = SpringUtil.getBean(factoryName, SqlSessionFactory.class);
        return dataSourceUtil.getTemplate(sqlSessionFactory);
    }

    /**
     * 数据源
     *
     * @return {@link AtomikosDataSourceBean}
     */
    public abstract AtomikosDataSourceBean dataSource();

    /**
     * sql会话工厂
     *
     * @return {@link SqlSessionFactory}
     * @throws Exception 异常
     */
    public abstract SqlSessionFactory sqlSessionFactory() throws Exception;

    /**
     * sql会议模板
     *
     * @return {@link SqlSessionTemplate}
     */
    public abstract SqlSessionTemplate sqlSessionTemplate();

}
