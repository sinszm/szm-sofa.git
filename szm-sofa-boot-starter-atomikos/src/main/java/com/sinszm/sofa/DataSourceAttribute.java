package com.sinszm.sofa;

import lombok.Data;

/**
 * 数据源属性
 *
 * @author admin
 */
@Data
public class DataSourceAttribute {

    /**
     * 驱动程序类
     * <p>
     *     $不能为空$
     * </p>
     */
    private String driverClassName;

    /**
     * 连接地址
     * <p>
     *     $不能为空$
     * </p>
     */
    private String url;

    /**
     * 账号
     * <p>
     *     $不能为空$
     * </p>
     */
    private String username;

    /**
     * 密码
     * <p>
     *     $不能为空$
     * </p>
     */
    private String password;

    /**
     * 映射器位置
     * <p>
     *     $不能为空$
     * </p>
     */
    private String mapperLocations;

    /**
     * 测试查询SQL
     */
    private String testQuerySql;

    /**
     * 自定义枚举包
     */
    private String typeEnumsPackage;

    private int minPoolSize = 5;
    private int maxPoolSize = 100;
    private int maxLifetime = 20000;
    private int borrowConnectionTimeout = 30;
    private int loginTimeout = 30;
    private int maintenanceInterval = 60;
    private int maxIdleTime = 60;

}
