package com.sinszm.sofa;

import com.baomidou.mybatisplus.autoconfigure.SpringBootVFS;
import lombok.Data;
import org.apache.ibatis.io.VFS;
import org.apache.ibatis.logging.Log;
import org.apache.ibatis.logging.stdout.StdOutImpl;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.LinkedHashMap;

/**
 * 数据源属性
 *
 * @author admin
 */
@Data
@ConfigurationProperties("sofa.atomikos")
public class DataSourceProperties {

    /**
     * 数据源的配置
     * <p>
     *     Key表示数据源名称；
     *     Value表示数据源配置属性；
     * </p>
     */
    private LinkedHashMap<String, DataSourceAttribute> db;

    private boolean enableLogicDelField = false;

    private String dbConfigLogicDeleteField = "del";

    private String dbConfigLogicDeleteValue = "1";

    private String dbConfigLogicNotDeleteValue = "0";

    private boolean pageOverflow = false;

    private long pageMaxLimit = 1000L;

    private boolean mybatisMapUnderscoreToCamelCase = true;

    private int mybatisDefaultStatementTimeout = 30;

    private int mybatisDefaultFetchSize = 100;

    private boolean mybatisCacheEnabled = false;

    private Class<? extends VFS> mybatisVfsImpl = SpringBootVFS.class;

    private Class<? extends Log> mybatisLogImpl = StdOutImpl.class;

    private boolean mybatisAggressiveLazyLoading = false;

    private boolean mybatisUseGeneratedShortKey = true;

}
